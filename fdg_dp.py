import json
import datetime as dt
from functools import partial
import findspark

findspark.init(spark_home='/opt/spark')
import pyspark
from pyspark.sql import functions as sf
from pyspark.sql.types import ArrayType, StructType, StructField, IntegerType, LongType, StringType
import pandas as pd

from comm.spark_utils import *
from comm.utils import *
from comm.mysql_handler import *

from config import *


def clear_dict(ddict):
    # remove those keys with corresponding values are None
    del_list = []
    for key, value in ddict.items():
        if value is None:
            del_list.append(key)
    for e in del_list:
        ddict.pop(e, None)
    return ddict

def gen_groupby_dict(groupby_list):
    groupby_dict = {}
    for key, value in groupby_list:
        groupby_dict[key] = value
    return groupby_dict


def gen_select_str(table_name, items):
    sel_str = ''
    if type(items) == str:
        sel_str = items
    elif type(items) == list:
        for item in items:
            sel_str += f'{item}, '
        sel_str = sel_str[:-2]
    sql_select_str = f'SELECT {sel_str} FROM {table_name}'
    return sql_select_str


def modify_player_id(player_id):
    try:
        new_player_id = f'P-{player_id}'
        return new_player_id
    except Exception as e:
        return ''


def modify_casino_id(casino_id):
    try:
        new_casino_id = f'C-{casino_id}'
        return new_casino_id
    except Exception as e:
        return ''


def time2day(time_str):
    try:
        day_str = time_str[:10]
        # print(day_str)
        return day_str
    except Exception as e:
        return ''


def get_weekday(day_str):
    try:
        tmp = time.strptime(day_str, "%Y-%m-%d %H:%M:%S")
        return int(tmp.tm_wday)
    except Exception as e:
        return -1


def get_time_section(time_str):
    try:
        # t2 = dt.time(18, 0, 0)
        hour = time.strptime(time_str, "%Y-%m-%d %H:%M:%S").tm_hour
        # print(hour//6)
        return hour // 6
    except Exception as e:
        return -1

def top_game_finder(a_list):
    return max(a_list, key=a_list.count)

def top_game_hold_finder(a_list):
    #print(a_list)
    for x in range(0,len(a_list)):
        a_list[x] = int(dt.datetime.strptime(a_list[x], '%Y-%m-%d %H:%M:%S').strftime("%s"))
    total_time = 0
    start = a_list[0]
    end = a_list[0]
    for cur in a_list[1:]:
        if cur - end < 900:
            total_time += end - start
            end = cur
        else:
            start = cur
            end = cur
    # for i in range(0,len(a_list)):
    #     if i+1 < len(a_list):
    #         if int(datetime.datetime.strptime(a_list[i+1], '%Y-%m-%d %H:%M:%S').strftime("%s")) - int(datetime.datetime.strptime(a_list[i], '%Y-%m-%d %H:%M:%S').strftime("%s")) < 900:
    #             total_time = total_time + int(datetime.datetime.strptime(a_list[i+1], '%Y-%m-%d %H:%M:%S').strftime("%s")) - int(datetime.datetime.strptime(a_list[i], '%Y-%m-%d %H:%M:%S').strftime("%s"))
    #         elif int(datetime.datetime.strptime(a_list[i+1], '%Y-%m-%d %H:%M:%S').strftime("%s")) - int(datetime.datetime.strptime(a_list[i], '%Y-%m-%d %H:%M:%S').strftime("%s")) > 900:
    #             total_time = total_time + 900
    #     total_time = total_time + 900
    return total_time


if __name__ == '__main__':
    debug = 0
    use_local_db = 0
    pp_table = 'player_profile'
    if use_local_db:
        # local db config
        db_host = '127.0.0.1'
        db_name = 'test_db'
        db_user = 'mysql'
        db_password = '*Abcd1234'

    else:
        # remote db config
        db_host = '10.10.33.43'
        db_name = 'growth_hacker_ga0'
        db_user = 'growth_hacker'
        db_password = 'growth_hacker'

    spark_conf = {
        'spark.io.compression.codec': 'snappy',
        # 'spark.io.compression.codec': 'lzf',
        "spark.cassandra.connection.host": "10.10.35.14",
        # "spark.default.parallelism": 20,
        "spark.driver.memory": "16g",
        # "spark.executor.memory": "2g",
        # "spark.python.worker.memory": "4g"
    }

    start_time = dt.datetime.now()

    # create player_profile table
    db_handler = MySqlHandler(host=db_host, username=db_user, password=db_password, db_name=db_name)
    db_handler.connect_db()
    if db_handler.sql_exec(f"show tables like '{pp_table}';") == 0:
        create_table_str = schema_to_create_table_sql(pp_table, player_profile_schema)
        db_handler.sql_exec(create_table_str)

    # initialize resources
    # spark = init_spark_session(app_name, master_url, spark_conf)
    spark = init_spark_session(app_name, spark_conf)
    # set log level to avoid too much information displayed
    # spark.sparkContext.setLogLevel("Error")

    # cargo_handler = MySqlHandler(host=cargo_host, username=cargo_user, password=cargo_password, db_name=cargo_name)
    # cargo_handler.connect_db()

    # marketing_events = cargo_handler.sql_select(f'SELECT * FROM marketing_events')
    # property_marketing_events = cargo_handler.sql_select(f'SELECT * FROM property_marketing_events')
    # rewards = cargo_handler.sql_select(f'SELECT * FROM rewards')
    # consumers = cargo_handler.sql_select(f'SELECT * FROM consumers')
    # game_rounds = cargo_handler.sql_select(f'SELECT * FROM game_rounds')

    # load game_recall_ex
    gre_sdf = spark_mysql_to_df(spark, db_host='10.10.33.43', user='growth_hacker', password='growth_hacker',
                                db_name='growth_hacker_ga0', db_table='game_recall_ex')
    pp_sdf = spark_mysql_to_df(spark, db_host=db_host, user=db_user, password=db_password,
                               db_name=db_name, db_table=pp_table)

    # limit data volume for testing and debugging
    if debug:
        # gre_sdf = gre_sdf.where(sf.col('started_at') < '2019-03-01')
        gre_sdf = gre_sdf.where((sf.col('started_at') > '2019-11-01') & (sf.col('started_at') < '2019-12-01'))
    gre_sdf.cache()
    
    gre_player = gre_sdf.groupBy('player_id')

    gre_sdf = gre_sdf.withColumn("player_id_str", sf.udf(modify_player_id)('player_id'))
    gre_sdf = gre_sdf.withColumn("casino_id_str", sf.udf(modify_casino_id)('casino_id'))
    gre_sdf = gre_sdf.withColumn("active_day", sf.udf(time2day)('started_at'))
    gre_sdf = gre_sdf.withColumn("week_day", sf.udf(get_weekday)('started_at'))
    gre_sdf = gre_sdf.withColumn("time_section", sf.udf(get_time_section)('started_at'))

    player_day_list = gre_sdf.groupBy('player_id', 'active_day').count().collect()
    player_day_dict = {}
    for player, day, cnt in player_day_list:
        if player not in player_day_dict:
            player_day_dict[player] = {}
        player_day_dict[player][day] = cnt


    # pd100
    pd100 = dt.date.today() - dt.timedelta(days=100)
    pd100_str = pd100.strftime('%Y-%m-%d %H:%M:%S')
    gre_pd100 = gre_sdf.where(sf.col('started_at') > pd100_str)
    pd100_active = gre_pd100.groupBy("player_id").agg(sf.collect_set('active_day')).collect()
    pd100_active_dict = gen_groupby_dict(pd100_active)
    pd100_time_section_list = gre_pd100.groupBy('player_id', 'time_section', 'active_day').count().collect()
    pd100_time_section_cnt = {}
    for player, time_section, active_day, round_cnt in pd100_time_section_list:
        if player not in pd100_time_section_cnt:
            pd100_time_section_cnt[player] = [[0, []], [0, []], [0, []], [0, []]]
        pd100_time_section_cnt[player][int(time_section)][0] += round_cnt
        pd100_time_section_cnt[player][int(time_section)][1].append(active_day)
    for player in pd100_time_section_cnt:
        for k in range(4):
            pd100_time_section_cnt[player][k][1] = len(set(pd100_time_section_cnt[player][k][1]))

    # update pd100
    for player in pd100_active_dict:
        update_dict = {}
        pd100_week_day_cnt = [0 for _ in range(7)]
        pd100_week_day_round_cnt = [0 for _ in range(7)]
        pd100_month_section_cnt = [0, 0, 0]
        pd100_month_section_round_cnt = [0, 0, 0]
        for day_str in pd100_active_dict[player]:
            tmp = time.strptime(day_str, "%Y-%m-%d")
            week_day = int(tmp.tm_wday)
            pd100_week_day_cnt[week_day] += 1
            pd100_week_day_round_cnt[week_day] += player_day_dict[player][day_str]
            month_sec = tmp.tm_mday // 10
            month_sec = min(month_sec, 2)
            pd100_month_section_cnt[month_sec] += 1
            pd100_month_section_round_cnt[month_sec] += player_day_dict[player][day_str]

        update_dict['pd100_wd0'] = pd100_week_day_cnt[0]
        update_dict['pd100_wd1'] = pd100_week_day_cnt[1]
        update_dict['pd100_wd2'] = pd100_week_day_cnt[2]
        update_dict['pd100_wd3'] = pd100_week_day_cnt[3]
        update_dict['pd100_wd4'] = pd100_week_day_cnt[4]
        update_dict['pd100_wd5'] = pd100_week_day_cnt[5]
        update_dict['pd100_wd6'] = pd100_week_day_cnt[6]

        update_dict['pd100_wd0_round'] = pd100_week_day_round_cnt[0]
        update_dict['pd100_wd1_round'] = pd100_week_day_round_cnt[1]
        update_dict['pd100_wd2_round'] = pd100_week_day_round_cnt[2]
        update_dict['pd100_wd3_round'] = pd100_week_day_round_cnt[3]
        update_dict['pd100_wd4_round'] = pd100_week_day_round_cnt[4]
        update_dict['pd100_wd5_round'] = pd100_week_day_round_cnt[5]
        update_dict['pd100_wd6_round'] = pd100_week_day_round_cnt[6]

        update_dict['pd100_m1'] = pd100_month_section_cnt[0]
        update_dict['pd100_m2'] = pd100_month_section_cnt[1]
        update_dict['pd100_m3'] = pd100_month_section_cnt[2]

        update_dict['pd100_m1_round'] = pd100_month_section_round_cnt[0]
        update_dict['pd100_m2_round'] = pd100_month_section_round_cnt[1]
        update_dict['pd100_m3_round'] = pd100_month_section_round_cnt[2]

        update_dict['pd100_t0'] = pd100_time_section_cnt[player][0][1]
        update_dict['pd100_t1'] = pd100_time_section_cnt[player][1][1]
        update_dict['pd100_t2'] = pd100_time_section_cnt[player][2][1]
        update_dict['pd100_t3'] = pd100_time_section_cnt[player][3][1]

        update_dict['pd100_t0_round'] = pd100_time_section_cnt[player][0][0]
        update_dict['pd100_t1_round'] = pd100_time_section_cnt[player][1][0]
        update_dict['pd100_t2_round'] = pd100_time_section_cnt[player][2][0]
        update_dict['pd100_t3_round'] = pd100_time_section_cnt[player][3][0]

        update_dict = clear_dict(update_dict)
        update_sql = dict_to_update_sql(pp_table, update_dict, f'WHERE player_id = {player}')
        print(update_sql)
        db_handler.sql_exec(update_sql)


    @sf.pandas_udf("long", functionType=sf.PandasUDFType.GROUPED_AGG)
    def cal_hold_time(time_series, span=900):
        ts_series = time_series.apply(lambda x: time.mktime(dt.datetime.strptime(x, '%Y-%m-%d %H:%M:%S').timetuple()))
        ts_series = ts_series.sort_values()
        ts_list = ts_series.tolist()

        sum_second = 0.0
        last_ts = ts_list[0]
        for cur_ts in ts_list[1:]:
            if cur_ts - last_ts < span:
                sum_second += cur_ts - last_ts
                last_ts = cur_ts
            else:
                last_ts = cur_ts
        # print(sum_second)
        return sum_second

    hold_time_list = gre_sdf.groupBy("player_id").agg(cal_hold_time(gre_sdf['started_at'])).collect()
    hold_time_dict = gen_groupby_dict(hold_time_list)

    player_ip_dict = {}
    player_ip_cnt_dict = {}
    player_ip_cnt = gre_sdf.groupBy('player_id', 'ip').count().collect()
    for player, ip, count in player_ip_cnt:
        if ip is None:
            continue
        elif (player not in player_ip_dict) or count > player_ip_cnt_dict[player]:
            player_ip_dict[player] = ip
            player_ip_cnt_dict[player] = count

    active_day_list = gre_sdf.groupBy('player_id', 'active_day').count().groupby('player_id').count().collect()
    active_day_dict = gen_groupby_dict(active_day_list)

    played_game_cnt_list = gre_sdf.groupBy('player_id', 'game_id').count().groupBy('player_id').count().collect()
    played_game_cnt_dict = gen_groupby_dict(played_game_cnt_list)

    # check which player_id needs to update
    # update_ts is null, update_ts < processed_at, new_player_id
    processed_at_list = gre_player.agg({'processed_at': 'max'}).collect()
    pp_list = pp_sdf.collect()
    pp_dict = {}
    if pp_list:
        for e in pp_list:
            if e['player_id']:
                pp_dict[e['player_id']] = e

   # columns which are not necessary for grouping
    first_play_list = gre_player.agg({'started_at': 'min'}).collect()
    last_play_list = gre_player.agg({'started_at': 'max'}).collect()
    max_bet_list = gre_player.agg({'bet_amt_cash': 'max'}).collect()
    avg_bet_list = gre_player.agg({'bet_amt_cash': 'avg'}).collect()
    total_bet_list = gre_player.agg({'bet_amt_cash': 'sum'}).collect()
    total_payout_list = gre_player.agg({'payout_amt_cash': 'sum'}).collect()
    cash_round_list = gre_player.agg({"id": 'count'}).collect()
    unit_payout_list = gre_sdf.withColumn('unit', gre_sdf['payout_amt_cash'] / gre_sdf['bet_amt_cash'])
    unit_payout_list = unit_payout_list.groupBy('player_id').agg({'unit': 'sum'}).collect()
    top_game = gre_sdf.groupBy("game_id",'player_id').agg({"game_id":"count"})
    top_game_list = top_game.groupBy('player_id',"game_id").agg({"count(game_id)": "max"}).drop("max(count(game_id))").collect()
    top_game_round_list = gre_sdf.groupBy("game_id", 'player_id').agg({"id": "count"}).drop('game_id').collect()

    processed_at_dict = gen_groupby_dict(processed_at_list)
    first_play_dict = gen_groupby_dict(first_play_list)
    last_play_dict = gen_groupby_dict(last_play_list)

    max_bet_dict = gen_groupby_dict(max_bet_list)
    avg_bet_dict = gen_groupby_dict(avg_bet_list)
    total_bet_dict = gen_groupby_dict(total_bet_list)
    total_payout_dict = gen_groupby_dict(total_payout_list)
    cash_round_dict = gen_groupby_dict(cash_round_list)
    unit_payout_dict = gen_groupby_dict(unit_payout_list)
    top_game_round_dict = gen_groupby_dict(top_game_round_list)
    top_game_dict = gen_groupby_dict(top_game_list)

    # process players which need update
    update_players = []
    for player, latest_update_ts in processed_at_list:
        if player not in pp_dict.keys():
            # create record of the new player
            db_handler.sql_exec(f'INSERT INTO {pp_table} (player_id) VALUES ({player})')
            update_players.append(player)
        elif pp_dict:
            if pp_dict[player]['update_ts'] is None or latest_update_ts > pp_dict[player]['update_ts']:
                update_players.append(player)

    # update players
    for k, update_player in enumerate(update_players):
        print(dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        print(f'processing No. {k + 1} of {len(update_players)},  {len(update_players) - k - 1} left ...')
        update_dict = {}

        update_dict['update_ts'] = processed_at_dict[update_player]
        update_dict['first_play'] = first_play_dict[update_player]
        tmp = gre_sdf.where(
            (sf.col('player_id') == update_player) & (sf.col('started_at') == first_play_dict[update_player])).take(1)
        update_dict['first_game_id'] = tmp[0].game_id
        update_dict['first_game_name'] = game_dict[tmp[0].game_id][0]
        update_dict['last_play'] = last_play_dict[update_player]
        tmp = gre_sdf.where(
            (sf.col('player_id') == update_player) & (sf.col('started_at') == last_play_dict[update_player])).take(1)
        update_dict['last_game_id'] = tmp[0].game_id
        update_dict['last_game_name'] = game_dict[tmp[0].game_id][0]
        update_dict['played_game_cnt'] = played_game_cnt_dict[update_player]

        update_dict['casino_id'] = tmp[0].casino_id
        update_dict['currency'] = tmp[0].currency
        update_dict['device'] = tmp[0].device_family
        update_dict['os'] = tmp[0].os_family
        update_dict['browser'] = tmp[0].browser_family

        if update_player in player_ip_dict:
            update_dict['ip'] = player_ip_dict[update_player]
            tmp = gre_sdf.where((sf.col('player_id') == update_player) & (sf.col('ip') == player_ip_dict[update_player])).sort('started_at').take(1)
            update_dict['ip_time'] = tmp[0].started_at
            update_dict['latitude'] = tmp[0].latitude
            update_dict['longitude'] = tmp[0].longitude
            update_dict['country'] = tmp[0].country
            update_dict['city'] = tmp[0].city

        update_dict['hold'] = hold_time_dict[update_player]
        update_dict['active_day'] = active_day_dict[update_player]

        update_dict['max_bet'] = max_bet_dict[update_player]
        update_dict['avg_bet'] = avg_bet_dict[update_player]
        update_dict['total_bet'] = total_bet_dict[update_player]
        update_dict['total_payout'] = total_payout_dict[update_player]
        update_dict['personal_profit'] = total_payout_dict[update_player] - total_bet_dict[update_player]
        update_dict['cash_round'] = cash_round_dict[update_player]
        update_dict['free_round'] = 0
        update_dict['unit_payout'] = unit_payout_dict[update_player]
        update_dict['personal_rtp'] = total_payout_dict[update_player] / total_bet_dict[update_player]
        update_dict['top1_game_id'] = top_game_dict[update_player]
        update_dict['top1_game_name'] = game_dict[top_game_dict[update_player]][0]
        update_dict['top1_game_round'] = top_game_round_dict[update_player]
        top_game_hold_list = gre_sdf.where((sf.col('player_id') == update_player) & (sf.col("game_id") == top_game_dict[update_player]))
        top_game_hold_list = top_game_hold_list.orderBy(top_game_hold_list['started_at'])
        top_game_hold_list = top_game_hold_list.groupBy("player_id").agg(sf.collect_list('started_at'))
        time_counter_udf = sf.udf(top_game_hold_finder, LongType())

        tmp = top_game_hold_list.withColumn("total", time_counter_udf(top_game_hold_list['collect_list(started_at)'])).take(1)
        update_dict['top1_game_hold'] = tmp[0].total

        update_dict = clear_dict(update_dict)
        update_sql = dict_to_update_sql(pp_table, update_dict, f'WHERE player_id = {update_player}')
        # update_sql = f'{update_sql} WHERE player_id = {update_player}'
        print(update_sql)
        db_handler.sql_exec(update_sql)

    # release resources
    db_handler.close_db()
    spark.stop()

    end_time = dt.datetime.now()
    print('Bingo')
    print(f'start time: {start_time}, end time: {end_time}, run_time: {end_time - start_time}')
